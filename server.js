var express = require('express');
var app = express();
var port = process.env.PORT || 3008;

var requestjson = require('request-json');

var bodyParser = require('body-parser');
app.use(bodyParser.json());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Request-Methods", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

var path = require('path');

var urlProyectoTUMovMlab = "https://api.mlab.com/api/1/databases/techu18proyectodbp/collections/Movimientos?apiKey=qJOr0akFh8-E5gBFyD4De96zfwBji7mN";
var urlProyectoTUCAMlab = "https://api.mlab.com/api/1/databases/techu18proyectodbp/collections/CotrolDeAcceso?apiKey=qJOr0akFh8-E5gBFyD4De96zfwBji7mN";

var clienteMlabCA = requestjson.createClient(urlProyectoTUCAMlab);
var clienteMlabMV = requestjson.createClient(urlProyectoTUMovMlab);

var crypto = require('crypto');
var hash = crypto.createHash('sha256').update('password').digest('base64');


app.listen(port);


console.log('todo listo RESTful API server started on: ' + port);

app.get("/",function (req, res){
  res.sendFile(path.join(__dirname,'index.html'));
})

/* Nuevos */

//Loing
app.put("/CA/PkLogin/",function (req,res){
  console.log("body: "+ req.body );
  var stringJSON = JSON.stringify(req.body )
  console.log("stringJSON: "+ stringJSON );
  console.log("body.usuario: "+ req.body.usuario );
  res.send("Hemos recibido su peticiòn actualizada");
})

app.post("/CA/Login",function (req, res){

  console.log("usuario: "+ req.body.usuario);
  console.log("password: "+ req.body.password);
  //var hashPwd = crypto.createHash('sha256').update(req.body.password).digest('base64');
  var hashPwd = crypto.createHash('sha256').update(req.body.password).digest('hex');
  var paramBusqueda = "{"+"\"usuario\""+":"+"{$eq:\""+req.body.usuario+"\"},"+
  "\"password\""+":"+"{$eq:\""+hashPwd+"\"}}";
  var paramCondicionaCampos = "{\"idcliente\":1}";

  console.log("Busqueda: "+ urlProyectoTUCAMlab+"&q="+paramBusqueda+"&f="+paramCondicionaCampos);
  clienteMlabCA = requestjson.createClient(urlProyectoTUCAMlab+"&q="+paramBusqueda+"&f="+paramCondicionaCampos);

  clienteMlabCA.get ('', function(err, resM, body) {
    if(err){
      console.log(body);
    }else{
      console.log("body: "+ body);
      if(body == null || body == ""){
        console.log("body es null o vacio: ");
         res.send(body);
       }
      else  {
        var stringJSON = JSON.stringify(body);
        stringJSON = stringJSON.substring(1,stringJSON.length-1);
        var obj = JSON.parse(stringJSON);
        console.log("body.idcliente: "+ obj.idcliente);
        var d = new Date();
        var idSession = d.getTime();
        console.log("idSession: "+ idSession);
        idSession =idSession+(1000*60*10);
        console.log("body.idcliente: "+ obj.idcliente);
        console.log("idSession: "+ idSession);
        var objJSONReq = { "$set" : { "sesion" : idSession } };
        paramCondicionaCampos = "{\"sesion\":1}";
        clienteMlabCA = requestjson.createClient(urlProyectoTUCAMlab+"&q="+paramBusqueda+"&f="+paramCondicionaCampos);
        console.log("segunda peticiòn.");
        clienteMlabCA.put ('', objJSONReq,function(err, resM, body) {
          if(err){
            console.log(body);
          }else{
            console.log(" Modificacion exitosa:: " + JSON.stringify(body));
          }
        })
        res.send(obj);
      }
    }
  })
})

//logout
app.get("/CA/Logout/:idcliente",function (req, res){

  console.log(" Cerrando sesion del idcliente: "+ req.params.idcliente);
  var paramBusqueda = "{"+"\"idcliente\""+":"+"{$eq:"+req.params.idcliente+"}}";
  var paramCondicionaCampos = "{\"sesion\":1}";
  var objJSONReq = { "$set" : { "sesion" : 0 } };
  console.log("Busqueda: "+ urlProyectoTUCAMlab+"&q="+paramBusqueda+"&f="+paramCondicionaCampos);
  clienteMlabCA = requestjson.createClient(urlProyectoTUCAMlab+"&q="+paramBusqueda+"&f="+paramCondicionaCampos);
  clienteMlabCA.put ('', objJSONReq, function(err, resM, body) {
    if(err){
      console.log(body);
    }else{
      res.send("Sesion finalizada.");
    }
  })

})


//Consulta movimientos

app.get("/MOV/:idcliente",function (req, res){

//Validad sesion
console.log("idcliente: "+ req.params.idcliente);
var paramBusqueda = "{"+"\"idcliente\""+":"+"{$eq:\""+req.params.idcliente+"\"}}";
var paramCondicionaCampos = "{\"sesion\":1}";
console.log("Busqueda: "+ urlProyectoTUCAMlab+"&q="+paramBusqueda+"&f="+paramCondicionaCampos);
clienteMlabCA = requestjson.createClient(urlProyectoTUCAMlab+"&q="+paramBusqueda+"&f="+paramCondicionaCampos);
clienteMlabCA.get ('', function(err, resM, body) {
  if(err){
    console.log(body);
  }else{
    console.log("body: "+ body);
    if(body == null || body == ""){
      console.log("body es null o vacio: ");
       res.send("No existe una sesion");
     }else{
        var stringJSON = JSON.stringify(body);
        stringJSON = stringJSON.substring(1,stringJSON.length-1);
        var obj = JSON.parse(stringJSON);
        console.log("body.sesion: "+ obj.sesion);
        if(obj.sesion > 0){
        console.log("Sesion valida");
        console.log("Consultando movimientos: " + urlProyectoTUMovMlab+"&q="+paramBusqueda);
        clienteMlabMV = requestjson.createClient(urlProyectoTUMovMlab+"&q="+paramBusqueda);
        clienteMlabMV.get ('', function(err, resM, body) {
            if(err){
              console.log(body);
              res.send("No exiten datos para el cliente: " + req.params.idcliente );
            }else{
              res.send(body);
            }
          })
        }else{
          res.send("No exiten sesion valida");
        }
      }
  }
})

})


//Agrega movimiento
app.post("/agregamovimiento",function (req, res){

  console.log("body: "+ req.body );
  var stringJSON = JSON.stringify(req.body );
  console.log("stringJSON req.body: "+ stringJSON );
  console.log("body.idcliente: "+ req.body.idcliente );
  console.log("body.Descripcion: "+ req.body.Descripcion );
  console.log("body.importe: "+ req.body.importe );
  console.log("body.fecha: "+ req.body.fecha );

  var paramBusqueda = "{"+"\"idcliente\""+":"+"{$eq:\""+req.body.idcliente +"\"}}";
  console.log("Consultando movimientos: " + urlProyectoTUMovMlab+"&q="+paramBusqueda);

  var JSONRequest = {  "$addToSet" :{
  				"listado": 	{
                  "fecha": req.body.fecha,
                  "importe": req.body.importe,
                  "Descripcion": req.body.Descripcion,
      		}
  	}
  };
  stringJSON = JSON.stringify(JSONRequest)
  console.log("JSONRequest : "+ stringJSON );

  clienteMlabMV = requestjson.createClient(urlProyectoTUMovMlab+"&q="+paramBusqueda);
  clienteMlabMV.put ('', JSONRequest,function(err, resM, body) {
    console.log(body);
    var codigoRespuesta;
    var mensajeRespuesta;
    if(err){
      codigoRespuesta = 0;
      mensajeRespuesta = "El movimiento no fue agregado";
    }else{
      codigoRespuesta = 1;
      mensajeRespuesta = "Movimiento agregado";
    }
    var JSONRequest = {
                    "codigo": codigoRespuesta,
                    "Descripcion": mensajeRespuesta
    };
    res.send(JSONRequest);
  })

})


//Agrega usuario
app.post("/agregausuario",function (req, res){

  console.log("body: "+ req.body );
  var stringJSON = JSON.stringify(req.body );
  console.log("stringJSON req.body: "+ stringJSON );

  console.log("body.usuario: "+ req.body.usuario );
  console.log("body.password: "+ req.body.password );
  console.log("body.nombre: "+ req.body.nombre );
  console.log("body.apellidop: "+ req.body.apellidop );
  console.log("body.apellidom: "+ req.body.apellidom );

  console.log("Agrega usuario: " + urlProyectoTUCAMlab);
  var numeroCliente = generaNumeroCliente();
  //var hashPwd = crypto.createHash('sha256').update(req.body.password).digest('base64');
  var hashPwd = crypto.createHash('sha256').update(req.body.password).digest('hex');

  var JSONRequest =  {
      "usuario": req.body.usuario,
      "password": hashPwd,
      "idcliente": numeroCliente,
      "sesion": 0
    };
  stringJSON = JSON.stringify(JSONRequest)
  console.log("JSONRequest : "+ stringJSON );

  clienteMlabCA = requestjson.createClient(urlProyectoTUCAMlab);
  clienteMlabCA.post ('', JSONRequest,function(err, resM, body) {
    console.log(body);
    var codigoRespuesta;
    var mensajeRespuesta;
    if(err){
      codigoRespuesta = 0;
      mensajeRespuesta = "El usuario no fue agregado";
    }else{
      codigoRespuesta = 1;
      mensajeRespuesta = "Usuario agregado";

      var d = new Date();
      var fechaApertura = d.getDate()+"/"+d.getMonth()+"/"+d.getFullYear();
      var JSONRequestMov = {
            "idcliente": numeroCliente,
            "nombre": req.body.nombre,
            "apellidoPaterno": req.body.apellidop,
            "apellidoMaterno": req.body.apellidom,
            "cuenta": generaCuenta(),
            "saldo": 0.0,
            "listado": [
                {
                    "fecha": fechaApertura,
                    "importe": 0.00,
                    "Descripcion": "Apertura de cuenta",
                }
            ]
        };
      stringJSON = JSON.stringify(JSONRequestMov)
      console.log("JSONRequestMov : "+ stringJSON );
      clienteMlabMV = requestjson.createClient(urlProyectoTUMovMlab);
      clienteMlabMV.post ('', JSONRequestMov,function(err, resM, body) {
        console.log(body);
      });
    var JSONRequest = {
                    "codigo": codigoRespuesta,
                    "Descripcion": mensajeRespuesta
    };
    res.send(JSONRequest);
    }
  })

})


/* */
function generaNumeroCliente(){
   var d = new Date();
   var cuenta = dosDigitos(d.getFullYear())+""+dosDigitos(d.getMonth())+""+dosDigitos(d.getDate())+""+dosDigitos(d.getHours())+""+dosDigitos(d.getMinutes())+""+dosDigitos(d.getSeconds());
   return parseInt(cuenta).toString(16).toUpperCase();
}

function generaCuenta(){
   var d = new Date();
   var cuenta = dosDigitos(d.getSeconds())+""+dosDigitos(d.getMinutes())+""+dosDigitos(d.getHours())+""+dosDigitos(d.getFullYear()+d.getMonth()+d.getDate());
   return cuenta;
}

function dosDigitos(numero){
   var d = ""+numero;
   while(d.length<2){
    d = "0"+ d;
   }
 return d;
}
/* Fin Nuevos */



app.post("/",function (req,res){
  console.log("params: "+ req.params.uno );
  console.log("body: "+ req.body );
  var stringJSON = JSON.stringify(req.body )
  console.log("stringJSON: "+ stringJSON );
  res.send("Hemos recibido su peticiòn actualizada");
})


app.put("/",function (req,res){
  res.send("Hemos recibido su peticiòn PUT");
})
